from setuptools import setup

setup(name='sri7s21816',
      version='0.1',
      description='thrift sri7 project',
      url='https://gitlab.com/s21816/sri7s21816',
      author='s2816',
      author_email='s21816@pjwstk.edu.pl',
      license='MIT',
      packages=['sri7s21816'],
      zip_safe=False)